import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'a[appExt]',
})
export class ExternalLinkDirective {
  @HostBinding('attr.target') targetAttr = '_blank';
}
