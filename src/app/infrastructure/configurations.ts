import { MatTooltipDefaultOptions } from '@angular/material/tooltip';

export const TooltipDefaultsConfiguration: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 0,
  touchendHideDelay: 0,
};
