import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appLowSeverity]',
})
export class LowSeverityDirective {
  @HostBinding('class') className = 'low-severity';
}
