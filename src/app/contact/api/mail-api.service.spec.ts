import { TestBed } from '@angular/core/testing';

import { MailApiService } from './mail-api.service';
import { HttpClientModule } from '@angular/common/http';

describe('MailApiService', () => {
  let service: MailApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientModule] });
    service = TestBed.inject(MailApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
