import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FactorChartComponent } from './factor-chart.component';

describe('FactorChartComponent', () => {
  let component: FactorChartComponent;
  let fixture: ComponentFixture<FactorChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FactorChartComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FactorChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
