import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SupportedRegion } from '../../configuration/region-select/region-select.component';

export interface FileAnalysisConfiguration {
  file: File;
  region: SupportedRegion;
}

@Component({
  selector: 'app-file-analysis-configuration',
  templateUrl: './file-analysis-configuration.component.html',
  styleUrls: ['./file-analysis-configuration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileAnalysisConfigurationComponent {
  @Output() analysisRequest = new EventEmitter<FileAnalysisConfiguration>();

  analysisForm: FormGroup = this.formBuilder.group({
    file: [undefined, Validators.required],
    region: [undefined, Validators.required],
  });

  constructor(private readonly formBuilder: FormBuilder) {}

  onSubmit(): void {
    this.analysisRequest.emit({ file: this.file(), region: this.region() });
  }

  private file(): File {
    return this.analysisForm.get('file')?.value;
  }

  private region(): SupportedRegion {
    return this.analysisForm.get('region')?.value;
  }
}
