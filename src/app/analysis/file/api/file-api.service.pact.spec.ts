import { FileApiService } from './file-api.service';
import { TestBed } from '@angular/core/testing';
import { Matchers, PactWeb } from '@pact-foundation/pact-web';
import { HttpClientModule } from '@angular/common/http';
import { SupportedRegion } from '../../configuration/region-select/region-select.component';

describe('FileApiService', () => {
  let provider: PactWeb;

  let service: FileApiService;

  beforeAll(() => {
    provider = new PactWeb();

    provider.removeInteractions();
  });

  afterAll((done) => {
    provider.finalize().then(done, done.fail);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [FileApiService],
    });
    service = TestBed.inject(FileApiService);
  });

  afterEach((done) => {
    provider.verify().then(done, (e) => done.fail(e));
  });

  describe('fetchFileAnalysis()', () => {
    beforeAll((done) => {
      provider
        .addInteraction({
          state: `provider accepts a new file analysis`,
          uponReceiving: 'a request to POST a file analysis request',
          withRequest: {
            method: 'POST',
            path: '/v0/files',
            headers: {
              'Content-Type': Matchers.string('multipart/form-data'),
            },
          },
          willRespondWith: {
            status: 200,
            body: Matchers.somethingLike({
              coefficients: [
                {
                  name: 'Intercept',
                  weight: 0.0545,
                  standardError: 0.288,
                  probability: 0.189,
                  confidenceStart: -0.408,
                  confidenceEnd: 0.0,
                },
                {
                  name: 'MKT',
                  weight: 1.0514,
                  standardError: 0.015,
                  probability: 0.0,
                  confidenceStart: 1.022,
                  confidenceEnd: 1.082,
                },
                {
                  name: 'SMB',
                  weight: 0.7467,
                  standardError: 0.034,
                  probability: 0.007,
                  confidenceStart: 0.679,
                  confidenceEnd: 0.813,
                },
                {
                  name: 'HML',
                  weight: 0.3088,
                  standardError: 0.044,
                  probability: 0.035,
                  confidenceStart: 0.223,
                  confidenceEnd: 0.389,
                },
                {
                  name: 'RMW',
                  weight: 0.2834,
                  standardError: 0.048,
                  probability: 0.0095,
                  confidenceStart: 0.189,
                  confidenceEnd: 0.37,
                },
                {
                  name: 'CMA',
                  weight: 0.0391,
                  standardError: 0.048,
                  probability: 0.416,
                  confidenceStart: -0.048,
                  confidenceEnd: 0.134,
                },
                {
                  name: 'WML',
                  weight: -0.1988,
                  standardError: 0.03,
                  probability: 0.075,
                  confidenceStart: -0.258,
                  confidenceEnd: -0.139,
                },
              ],
              confidenceLevel: 0.95,
              adjustedRSquared: 0.9698,
              observations: 314,
              excessReturn: 0.425,
            }),
            headers: {
              'Content-Type': 'application/json',
            },
          },
        })
        .then(done, (error) => done.fail(error));
    });

    it('should fetch analysis report', (done) => {
      service
        .fetchAnalysisReport({
          file: new File([], 'test.csv'),
          region: SupportedRegion.ofType('DEVELOPED-EX-US'),
        })
        .subscribe(
          (response) => {
            expect(response.confidenceLevel).toEqual(0.95);
            expect(response.observations).toEqual(314);
            expect(response.excessReturn).toEqual(0.425);
            expect(response.adjustedRSquared).toEqual(0.9698);

            expect(
              response.coefficients.find((value) => value.name === 'MKT')
                ?.weight
            ).toEqual(1.0514);
            expect(
              response.coefficients.find((value) => value.name === 'MKT')
                ?.standardError
            ).toEqual(0.015);
            expect(
              response.coefficients.find((value) => value.name === 'MKT')
                ?.probability
            ).toEqual(0.0);

            expect(
              response.coefficients.find((value) => value.name === 'SMB')
                ?.weight
            ).toEqual(0.7467);
            expect(
              response.coefficients.find((value) => value.name === 'HML')
                ?.weight
            ).toEqual(0.3088);
            expect(
              response.coefficients.find((value) => value.name === 'RMW')
                ?.weight
            ).toEqual(0.2834);
            expect(
              response.coefficients.find((value) => value.name === 'CMA')
                ?.weight
            ).toEqual(0.0391);
            expect(
              response.coefficients.find((value) => value.name === 'CMA')
                ?.probability
            ).toEqual(0.416);
            expect(
              response.coefficients.find((value) => value.name === 'WML')
                ?.weight
            ).toEqual(-0.1988);

            expect(
              response.coefficients.find((value) => value.name === 'Intercept')
                ?.weight
            ).toEqual(0.0545);

            done();
          },
          (error) => done.fail(error)
        );
    });
  });
});
