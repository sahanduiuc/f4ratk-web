import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestBarComponent } from './request-bar/request-bar.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [RequestBarComponent],
  imports: [
    CommonModule,
    MatProgressBarModule,
    MatDividerModule,
    MatCardModule,
    MatIconModule,
    FlexLayoutModule,
  ],
  exports: [RequestBarComponent],
})
export class SharedModule {}
