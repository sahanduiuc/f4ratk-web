import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { CostApiService, CostBreakdownResponse } from './api/cost-api.service';
import { Statements } from './report-statements-form/report-statements-form.component';

@Component({
  selector: 'app-cost',
  templateUrl: './cost.component.html',
  styleUrls: ['./cost.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CostComponent {
  costBreakdown?: CostBreakdownResponse;

  isAnalyzing = false;

  constructor(
    private readonly costApiService: CostApiService,
    private readonly cdr: ChangeDetectorRef
  ) {}

  analyze(statements: Statements): void {
    this.isAnalyzing = true;

    this.costApiService
      .fetchCostBreakdown({
        totalExpenseRatio: statements.totalExpenseRatio,
        expenses: statements.expenses,
        withholdingTax: statements.withholdingTax,
        transactionCosts: statements.transactionCosts,
        capitalGainsTax: statements.capitalGainsTax,
        securitiesLendingIncome: statements.securitiesLendingIncome,
      })
      .subscribe({
        next: (response: CostBreakdownResponse) =>
          this.displayCostBreakdown(response),
        error: () => this.clearCostBreakdown(),
      });
  }

  private displayCostBreakdown(costBreakdown: CostBreakdownResponse): void {
    this.costBreakdown = costBreakdown;
    this.isAnalyzing = false;
    this.cdr.markForCheck();
  }

  private clearCostBreakdown(): void {
    this.costBreakdown = undefined;
    this.isAnalyzing = false;
    this.cdr.markForCheck();
  }
}
