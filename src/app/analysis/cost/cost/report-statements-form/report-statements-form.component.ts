import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';

export interface Statements {
  totalExpenseRatio: number;
  expenses: number;
  transactionCosts?: number;
  withholdingTax?: number;
  capitalGainsTax?: number;
  securitiesLendingIncome?: number;
}

type ControlName =
  | 'totalExpenseRatio'
  | 'expenses'
  | 'transactionCosts'
  | 'withholdingTax'
  | 'capitalGainsTax'
  | 'securitiesLendingIncome';

const onlyGreaterThanZero =
  (): ValidatorFn =>
  (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;

    if (value === '' || Number(value) > 0) {
      return null;
    }

    return { onlyGreaterThanZero: true };
  };

@Component({
  selector: 'app-report-statements-form',
  templateUrl: './report-statements-form.component.html',
  styleUrls: ['./report-statements-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportStatementsFormComponent implements OnInit {
  private static readonly onlyNumber = Validators.pattern('\\d+(\\.\\d+)?');
  private static readonly onlyInteger = Validators.pattern('\\d+');

  @Output() analysisRequest = new EventEmitter<Statements>();

  statementsForm: FormGroup = this.formBuilder.group({
    totalExpenseRatio: [
      undefined,
      [
        Validators.required,
        ReportStatementsFormComponent.onlyNumber,
        onlyGreaterThanZero(),
      ],
    ],
    expenses: [
      undefined,
      [
        Validators.required,
        ReportStatementsFormComponent.onlyInteger,
        Validators.min(1),
      ],
    ],
    transactionCosts: [undefined, ReportStatementsFormComponent.onlyInteger],
    withholdingTax: [undefined, ReportStatementsFormComponent.onlyInteger],
    capitalGainsTax: [undefined, ReportStatementsFormComponent.onlyInteger],
    securitiesLendingIncome: [
      undefined,
      ReportStatementsFormComponent.onlyInteger,
    ],
  });

  constructor(private readonly formBuilder: FormBuilder) {}

  severity(controlName: ControlName): 'high-severity' | 'low-severity' {
    return this.control(controlName).valid ? 'low-severity' : 'high-severity';
  }

  hasError(controlName: ControlName): boolean {
    return this.control(controlName).errors != null;
  }

  onSubmit(): void {
    this.analysisRequest.emit(this.statements());
  }

  ngOnInit(): void {}

  private statements(): Statements {
    return {
      totalExpenseRatio: this.input('totalExpenseRatio'),
      expenses: this.input('expenses'),
      transactionCosts: this.input('transactionCosts'),
      withholdingTax: this.input('withholdingTax'),
      capitalGainsTax: this.input('capitalGainsTax'),
      securitiesLendingIncome: this.input('securitiesLendingIncome'),
    };
  }

  private input(controlName: ControlName): number {
    return Number(this.control(controlName).value);
  }

  private control(controlName: ControlName): AbstractControl {
    const control = this.statementsForm.get(controlName);

    if (!control) {
      throw new RangeError();
    }

    return control;
  }
}
