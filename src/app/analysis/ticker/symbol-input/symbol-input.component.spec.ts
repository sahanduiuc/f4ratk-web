import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SymbolInputComponent } from './symbol-input.component';
import { FormControl, FormGroup } from '@angular/forms';

describe('SymbolInputComponent', () => {
  let component: SymbolInputComponent;
  let fixture: ComponentFixture<SymbolInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SymbolInputComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SymbolInputComponent);
    component = fixture.componentInstance;
    component.formGroup = new FormGroup({ symbol: new FormControl() });
    component.formName = 'symbol';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
