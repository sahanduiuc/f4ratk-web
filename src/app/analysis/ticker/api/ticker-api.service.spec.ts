import { TestBed } from '@angular/core/testing';

import { TickerApiService } from './ticker-api.service';
import { HttpClientModule } from '@angular/common/http';

describe('TickerApiService', () => {
  let service: TickerApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(TickerApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
