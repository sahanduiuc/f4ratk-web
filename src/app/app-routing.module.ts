import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { TickerComponent } from './analysis/ticker/ticker.component';
import { FileComponent } from './analysis/file/file.component';
import { LegalNoticeComponent } from './legal-notice/legal-notice.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { HelpComponent } from './help/help.component';
import { CostComponent } from './analysis/cost/cost/cost.component';

const routes: Routes = [
  {
    path: 'tickers',
    component: TickerComponent,
  },
  {
    path: 'tickers/:config',
    component: TickerComponent,
  },
  {
    path: 'files',
    component: FileComponent,
  },
  {
    path: 'costs',
    component: CostComponent,
  },
  {
    path: 'help',
    component: HelpComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
  },
  {
    path: 'privacy-policy*',
    component: PrivacyPolicyComponent,
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'legal-notice',
    component: LegalNoticeComponent,
  },
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'about',
    pathMatch: 'full',
  },
];

const config: ExtraOptions = {
  anchorScrolling: 'enabled',
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
