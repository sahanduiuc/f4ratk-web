import { ChangeDetectionStrategy, Component } from '@angular/core';
import { environment } from '../../environments/environment';
import { PlatformLocation } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-legal-notice',
  templateUrl: './legal-notice.component.html',
  styleUrls: ['./legal-notice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LegalNoticeComponent {
  readonly name: string = environment.contact.name;
  readonly address: string = environment.contact.address;
  readonly city: string = environment.contact.city;
  readonly country: string = environment.contact.country;
  readonly email: string = environment.contact.email;

  readonly contactFormUrl: string;

  constructor(
    private readonly platformLocation: PlatformLocation,
    private readonly router: Router
  ) {
    this.contactFormUrl =
      (this.platformLocation as any).location.origin +
      router.createUrlTree(['/contact']);
  }
}
