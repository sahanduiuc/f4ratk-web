import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { TickerComponent } from './analysis/ticker/ticker.component';
import { FileComponent } from './analysis/file/file.component';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { FooterComponent } from './footer/footer.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { PlotlyViaWindowModule } from 'angular-plotly.js';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { LegalNoticeComponent } from './legal-notice/legal-notice.component';
import { HeaderComponent } from './header/header.component';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { QuoteTableComponent } from './analysis/file/quote-table/quote-table.component';
import { ExternalLinkDirective } from './infrastructure/external-link.directive';
import { ReportComponent } from './analysis/report/report.component';
import { FileAnalysisConfigurationComponent } from './analysis/file/file-analysis-configuration/file-analysis-configuration.component';
import { FileSelectComponent } from './analysis/file/file-analysis-configuration/file-select/file-select.component';
import { MatSelectModule } from '@angular/material/select';
import { RegionSelectComponent } from './analysis/configuration/region-select/region-select.component';
import { CurrencySelectComponent } from './analysis/configuration/currency-select/currency-select.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { SymbolInputComponent } from './analysis/ticker/symbol-input/symbol-input.component';
import { ReportTableComponent } from './analysis/report/report-table/report-table.component';
import { FactorChartComponent } from './analysis/report/factor-chart/factor-chart.component';
import { MailFormComponent } from './contact/mail-form/mail-form.component';
import { LowSeverityDirective } from './infrastructure/low-severity.directive';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HelpComponent } from './help/help.component';
import {
  MAT_TOOLTIP_DEFAULT_OPTIONS,
  MatTooltipModule,
} from '@angular/material/tooltip';
import { TooltipDefaultsConfiguration } from './infrastructure/configurations';
import { ErrorsComponent } from './analysis/report/errors/errors.component';
import { CostModule } from './analysis/cost/cost.module';

@NgModule({
  declarations: [
    AppComponent,
    TickerComponent,
    FileComponent,
    FooterComponent,
    ReportTableComponent,
    FactorChartComponent,
    LegalNoticeComponent,
    HeaderComponent,
    NavComponent,
    HelpComponent,
    AboutComponent,
    ContactComponent,
    PrivacyPolicyComponent,
    QuoteTableComponent,
    ReportComponent,
    FileAnalysisConfigurationComponent,
    FileSelectComponent,
    RegionSelectComponent,
    CurrencySelectComponent,
    SymbolInputComponent,
    MailFormComponent,
    ExternalLinkDirective,
    LowSeverityDirective,
    ErrorsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    MatIconModule,
    MatTableModule,
    PlotlyViaWindowModule,
    MatCardModule,
    MatTabsModule,
    LayoutModule,
    MatExpansionModule,
    MatSelectModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatTooltipModule,
    CostModule,
  ],
  providers: [
    {
      provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
      useValue: TooltipDefaultsConfiguration,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
