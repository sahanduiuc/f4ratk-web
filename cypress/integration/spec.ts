describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/');
    cy.url().should('eq', Cypress.config().baseUrl + 'about');
    cy.title().should('equal', 'f4ratk');
    cy.contains('Here be dragons');
  });
});
